# Pillars

![Bitbucket Version](https://img.shields.io/badge/pod-v0.0.8-brightgreen.svg)
![Platform](https://img.shields.io/badge/platform-iOS-lightgrey.svg)
![License](https://img.shields.io/badge/license-MIT-000000.svg)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Pillars is available through [CocoaPods](http://cocoapods.org). To install
it, first add these lines at the very beginning of your podfile:
```ruby
source 'https://bitbucket.org/eggon/ios-cocoapods-private-trunk.git'
source 'https://github.com/CocoaPods/Specs.git'
```

Then just import the pod as usual:
```ruby
pod 'Pillars'
```

## Features

- RobotToast: simple non-blocking popup similar to Android's Toast.
- TimeWatch: helper class for time-based benchmarks.
- Keychain: keychain wrapper to save the user's token.
- UIAlertController extensions
- UIViewController extensions
- String extensions
- Error/NSError extensions
- Date/Formatter/String extensions
- Collection aliases

## To implement

- Optional extensions (JuniorBit)
- Alamofire extension to stop all requests (JuniorBit)
- Login/signup flow view models (Morellato)
- isBlank and similar functions from Kotlin
- Functions that could come in handy from Eggoid
- dropShadow(offset: CGSize, opacity: CGFloat, radius: CGFloat) (see Advocatively's MainCell.swift)

## How to update and push changes

If you update any of the library's functionalities and you want to push the update, you need to follow these steps:

- Update the Pillars.podspec's s.version
- Push your changes to master
- Create a tag on the remote master branch with the same version as the Pillars.podspec
- With the terminal, navigate to the library's folder
- If you don't have the master trunk in your local cocoapods already, run `pod repo add PrivateTrunk https://bitbucket.org/eggon/ios-cocoapods-private-trunk.git`
- Run `pod repo push PrivateTrunk Pillars.podspec`

## Author

Mattia Contin, m.contin@eggonemail.com

## License

Pillars is available under the MIT license. See the LICENSE file for more info.
