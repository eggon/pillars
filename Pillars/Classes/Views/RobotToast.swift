//
//  RobotToast.swift
//  Pillars
//
//  Created by Mattia on 10/01/2018.
//

import UIKit
import MBProgressHUD

/// Wrapper for MBProgressHUD that tries to replicate Android's Toast behaviour.
/// Usage:
/// RobotToast.setup().show(`in`: view, withMessage: "This is a toast")
open class RobotToast {
    
    /// Content margin
    open static let defaultMargin: CGFloat = 10
    /// Time in seconds after which the toast must be hidden
    open static let defaultDelay: Double = 2
    
    /// Helper function to setup custom values to the Toast, allows chaining by returning the instance
    open static func setup(margin: CGFloat = defaultMargin, hideAfter: Double = defaultDelay) -> MBProgressHUD {
        let hud = MBProgressHUD()
        hud.isUserInteractionEnabled = false
        hud.mode = .text
        hud.label.adjustsFontSizeToFitWidth = true
        hud.label.numberOfLines = 2
        hud.label.minimumScaleFactor = 0.8
        hud.margin = defaultMargin
        hud.removeFromSuperViewOnHide = true
        return hud
    }
    
    /// Quicker way to display a Toast, no setup/chaining but uses the default values, which are usually enough
    open static func showIn(view: UIView, message: String) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.isUserInteractionEnabled = false
        hud.mode = .text
        hud.label.adjustsFontSizeToFitWidth = true
        hud.label.numberOfLines = 2
        hud.label.minimumScaleFactor = 0.8
        hud.label.text = message
        hud.margin = defaultMargin
        hud.removeFromSuperViewOnHide = true
        hud.offset.y = (UIDevice.current.userInterfaceIdiom == .pad) ? view.frame.size.height / 4 : view.frame.size.height / 3
        hud.hide(animated: true, afterDelay: defaultDelay)
    }
    
}

extension MBProgressHUD {
    
    /// Shows the Toast
    open func show(`in` view: UIView, withMessage message: String, `for`: Double = RobotToast.defaultDelay) {
        label.text = message
        offset.y = (UIDevice.current.userInterfaceIdiom == .pad) ? view.frame.size.height / 4 : view.frame.size.height / 3
        show(animated: true)
        hide(animated: true, afterDelay: `for`)
    }
    
}
