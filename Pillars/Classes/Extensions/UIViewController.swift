//
//  UIViewController.swift
//  Pillars
//
//  Created by Mattia on 10/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

import UIKit
import MBProgressHUD
//import RxSwift
//import Crashlytics

/// Helpers for controllers.
/// Will be completed when Swift 4.1 is released because we need #if canImport
extension UIViewController {
    
    /// Set this view controller as root with an animation
    public func setRoot(with animation: UIViewAnimationOptions = .transitionCurlUp) {
        guard let w = UIApplication.shared.delegate?.window, let window = w else { return }
        
        UIView.transition(with: window,
                          duration: 0.5,
                          options: animation,
                          animations: { window.rootViewController = self })
    }
    
    /// Show a blocking spinner to block interaction during long operations
    public func showSpinner() {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    /// Hides any spinnger that is being shown
    public func hideSpinner() {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    /// Swift 4.1 needed to implement this correctly with #if canImport(RxSwift)
//    #if canImport(RxSwift)
//    func showSpinnerRx() -> Completable {
//        return Completable.create { [weak self] completable in
//            if let this = self { this.showSpinner() }
//            completable(.completed)
//            return Disposables.create()
//        }
//    }
//    #endif
    
    /// Swift 4.1 needed to implement this correctly with #if canImport(RxSwift)
//    #if canImport(RxSwift)
//    func hideSpinnerRx() -> Completable {
//        return Completable.create { [weak self] completable in
//            if let this = self { this.hideSpinner() }
//            completable(.completed)
//            return Disposables.create()
//        }
//    }
//    #endif
    
}

extension UIViewController {
    
//    func warnAndLogout() {
//        showAlert(withTitle: "Errore",
//                  message: "Sessione scaduta. Verrai riportato alla schermata di login.",
//                  then: { _ in
//                    self.logout()
//        })
//    }
    
//    func logout() {
//        Keychain.shared.logout()
//        let vc = UIStoryboard(name: "Access", bundle: nil).instantiateInitialViewController()
//        vc?.setRoot()
//    }
    
    /// This should be implemented by every app
//    func wipeAndLogout() {
//        UserData.wipe()
//        LocalStore.wipe()
//        logout()
//    }
    
}

/// Alert showing helpers, this piece of code is here just to be copy-pasted in our projects and implemented correctly with translated strings
extension UIViewController {
    
//        func showAlert(fromError error: Error) {
//            showAlert(fromNSError: error as NSError)
//        }
    
//        func showAlert(fromNSError error: NSError) {
//            showAlert(withTitle: "Errore", message: error.localizedDescription)
//        }
    
//        func showAlert(withTitle title: String, message: String, then: ((UIAlertAction) -> Void)? = nil, cancelable: Bool = false) {
//            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: then))
//            if cancelable {
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
//            }
//            present(alert, animated: true, completion: nil)
//        }
    
}

extension UIViewController {
    
//    func handle(error: Error) {
//        let nsError = error as NSError
//        Crashlytics.sharedInstance().recordError(nsError)
//
//        switch nsError.code {
//        case 401:
//            warnAndLogout()
//        default:
//            showAlert(fromError: error)
//        }
//    }
    
}

extension UIViewController {
    
    /// Enables hiding the keyboard when the user taps around
    open func hideKeyboardWhenTappingAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    /// Hides the keyboard. Overridable in case the user needs to add functionalities
    @objc open func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
