//
//  Errors.swift
//  Pillars
//
//  Created by Mattia on 12/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

extension Error {
    
    /// Return this as NSError
    public var nsError: NSError {
        return self as NSError
    }
    
    /// Returns the NSError code
    public var code: Int {
        return nsError.code
    }
    
    /// Returns the NSError message
    public var message: String {
        return nsError.localizedDescription
    }
    
}

extension NSError {
    
    /// Helper function to build errors with custom messages more easily
    public static func with(message: String, code: Int = -1) -> NSError {
        return NSError(domain: Bundle.main.bundleIdentifier ?? "co.eggon",
                       code: code,
                       userInfo: [NSLocalizedDescriptionKey: message])
    }
    
}

/// TODO:
/// wait for Swift 4.1 -> #if canImport("Moya")
//extension Error {

//    public var moyaError: MoyaError {
//        return error as? MoyaError
//    }
    
//}

