//
//  UIAlertController.swift
//  Pillars
//
//  Created by Mattia on 10/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

/// Helper builder functions to setup an alert more quickly
extension UIAlertController {
    
    
    @discardableResult
    public func set(title: String? = nil, message: String? = nil) -> UIAlertController {
        self.title = title
        self.message = message
        return self
    }
    
    @discardableResult
    public func setConfirm(title: String, action: (() -> Void)? = nil) -> UIAlertController {
        let a = UIAlertAction(title: title, style: .default, handler: { _ in action?() })
        addAction(a)
        return self
    }
    
    @discardableResult
    public func setCancel(title: String, action: (() -> Void)? = nil) -> UIAlertController {
        let a = UIAlertAction(title: title, style: .cancel, handler: { _ in action?() })
        addAction(a)
        return self
    }
    
}
