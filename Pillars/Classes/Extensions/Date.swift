//
//  Date.swift
//  Pillars
//
//  Created by Mattia on 12/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

extension Formatter {
    
    /// Formatter for iso8601 dates
    public static var iso8601: DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(abbreviation: "CEST")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formatter
    }
    
}

extension Date {
    
    /// Returns this as iso8601 string
    public var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
    
}

extension String {
    
    /// Return a date if this string is in iso8601
    public var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)
    }
    
}

/// Need Swift 4.1 #if canImport("ObjectMapper")
//extension DateTransform {
//
//    static var ISO8601Transformer = {
//        return TransformOf<Date, String>(fromJSON: { $0?.dateFromISO8601 }, toJSON: { $0.map { $0.iso8601 } })
//    }()
//
//}
