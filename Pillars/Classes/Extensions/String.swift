//
//  String.swift
//  Pillars
//
//  Created by Mattia on 10/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

/// Helper extensions for Moya
extension String {
    
    /// Quickly converts a String to URL, lets the user decide if it's safe to unwrap
    public var toUrl: URL? {
        return URL(string: self)
    }
    
    /// Returns self with escaped chars to be used as URL
    public var urlEscaped: String? {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
    }
    
    /// Returns self as UTF8 encoded data
    public var utf8Encoded: Data? {
        return data(using: .utf8)
    }
    
}
