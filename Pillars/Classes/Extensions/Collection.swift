//
//  Collection.swift
//  Pillars
//
//  Created by Mattia on 12/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

/// Helper functions for faster checking of collection emptiness
extension Collection {
    
    public var isNotEmpty: Bool {
        return !isEmpty
    }
    
    public var hasElements: Bool {
        return isNotEmpty
    }
    
}
