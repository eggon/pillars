//
//  Pillar.swift
//  Pillars
//
//  Created by Mattia on 27/12/2017.
//

import Foundation

open class Pillar {
    
    open static func stand() {
        print("Standing")
    }
    
}
