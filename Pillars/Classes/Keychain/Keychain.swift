//
//  Keychain.swift
//  Pillars
//
//  Copyright © 2017 @ventuz. All rights reserved.
//

fileprivate enum Keys {
    static let accessTokenKey = "AccessToken"
}

public class Keychain {
    
    public static let shared = Keychain()
    
    public static var prefix = "default"
    
    private let keychain = KeychainSwift(keyPrefix: prefix)
    
    /// Saves the passed token in the keychain.
    /// - returns: True if the token was successfully saved.
    @discardableResult
    public func save(accessToken: String) -> Bool {
        return keychain.set(accessToken, forKey: Keys.accessTokenKey)
    }
    
    /// Deletes the token from the keychain.
    /// - returns: True if the token was successfully deleted.
    private func deleteAccessToken() -> Bool {
        return keychain.delete(Keys.accessTokenKey)
    }
    
    /// Returns the token or nil if nothing was saved.
    public var accessToken: String? {
        return keychain.get(Keys.accessTokenKey)
    }
    
    /// Ends the current session.
    /// - returns: True if the user was successfully logged out, false otherwise.
    @discardableResult
    public func logout() -> Bool {
        return deleteAccessToken()
    }
    
}
