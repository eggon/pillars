//
//  TimeWatch.swift
//  Pillars
//
//  Created by Mattia on 15/01/2018.
//  Copyright © 2018 co.eggon. All rights reserved.
//

import Foundation

/**
 This is a utility class to run time-based benchmarks.
 Basic usage:
 
 TimeWatch.start(for: "my-key")
 longOperation()
 TimeWatch.printLap(for: "my-key")
 */
public struct TimeWatch {
    
    private static var records: [String: [Date]] = [:]
    
    private init() { }
    
    /**
     Records a starting date for the given key.
     If a previous benchmark was already initiated, the records are cleared.
     */
    public static func start(for key: String) {
        //        if var records = records[key], records.hasElements {
        debugPrint("⏱ A new benchmark is starting for \(key) ⏱")
        //            records = []
        //        }
        
        records[key] = [Date()]
    }
    
    /**
     Registers a lap for the given key and returns the time between the current time and the last record.
     If no records for the key are found, nothing is registered and -1 is returned.
     */
    @discardableResult
    public static func lap(for key: String) -> Double {
        if var records = records[key], !records.isEmpty {
            let last = Date()
            records.append(last)
            let time = last.timeIntervalSince(records[records.count - 2])
            return time
        }
        return -1
    }
    
    /**
     Registers a lap for the given key and prints the last lap time.
     If no key is found, a warning message is shown.
     */
    public static func printLap(for key: String) {
        let lapTime = lap(for: key)
        if lapTime > -1 {
            return debugPrint("⏱ \(key) took \(lapTime)s since last lap ⏱")
        }
        return debugPrint("⏱ No TimeWatch running for \(key) ⏱")
    }
    
    /**
     Returns the difference between the first and the last record for the given key.
     */
    public static func total(for key: String) -> Double {
        if let records = records[key] {
            if records.count > 1 {
                let start = records.first!
                let end = records.last!
                let total = end.timeIntervalSince(start)
                return total
            } else if records.count == 1 {
                let start = records.first!
                let end = Date()
                let total = end.timeIntervalSince(start)
                return total
            } else {
                return 0
            }
        }
        return -1
    }
    
    /**
     Prints the total recorded time for the given key.
     This function doesn't add a lap.
     */
    public static func printTotal(for key: String) {
        let lapTime = total(for: key)
        if lapTime > -1 {
            return debugPrint("⏱ \(key) took \(lapTime)s since starting ⏱")
        }
        return debugPrint("⏱ No TimeWatch running for \(key) ⏱")
    }
    
    /**
     Resets the recording for a given key.
     */
    public static func reset(key: String) {
        records[key] = []
    }
    
}

